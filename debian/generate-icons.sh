#!/bin/sh
install -Dm644 usr/share/icons/hicolor/scalable/apps/synaptic.svg \
	debian/synaptic-tweaks/usr/share/icons/hicolor/scalable/apps/synaptic.svg

# Convert svg into png of different sizes
sizes="8 16 22 24 32 36 42 48 64 72 96 128 192 256 512 1024"
for size in $sizes; do
	mkdir -p debian/synaptic-tweaks/usr/share/icons/hicolor/${size}x${size}/apps
	inkscape --export-filename=debian/synaptic-tweaks/usr/share/icons/hicolor/${size}x${size}/apps/synaptic.png \
		-w ${size} -h ${size} debian/synaptic-tweaks/usr/share/icons/hicolor/scalable/apps/synaptic.svg;
done
